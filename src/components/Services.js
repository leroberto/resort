import React, { Component } from 'react'
import Title from './Title'
import {FaCocktail,FaHiking,FaShuttleVan,FaBeer} from 'react-icons/fa'

export default class Services extends Component {

    state = {
        services : [
            {
                icon : <FaCocktail/>,
                title: 'Free CockTails',
                info: "She put his pistol down, picked up her fletcher, dialed the barrel over to single shot, and very carefully put a toxin dart through the center of a junked console."                
            },
            {
                icon : <FaHiking/>,
                title: 'Endless Hicking',
                info: "Images formed and reformed: a flickering montage of the Sprawl’s towers and ragged Fuller domes, dim figures moving beyond the hull."                
            },
            {
                icon : <FaShuttleVan/>,
                title: 'Free Vans',
                info: "She put his pistol down, picked up her fletcher and dialed the barrel over to single shot."                
            },
            {
                icon : <FaBeer/>,
                title: 'Strongest',
                info: "She put his pistol down and picked up her fletcher."                 
            }
        ]
    }

    render() {
        return (
            <section className="services">
        <Title title="Services" />
        <div className="services-center">
        {this.state.services.map((item,index) => {
            return (
                <article key={index} className="service">
                    <span>{item.icon}</span>
                    <h6>{item.title}</h6>                    
                    <p>{item.info}</p>
                </article>
            )
        })}    
        </div>                    
            </section>
        )
    }
}
